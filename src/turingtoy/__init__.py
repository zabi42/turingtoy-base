from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)



from typing import Tuple, List, Optional

def moveCurosr(direction, cursor_position):
    return cursor_position -1 if direction == "L" else cursor_position+1


def run_turing_machine(machine: Dict, input_: str, steps: Optional[int] = None, ) -> Tuple[str, List, bool]:
    
    #output variable
    history = []
    isAccepted = True
    tape: list[str] = list(input_)


    states = machine["table"].keys()
    cursor_position = 0
    blank = machine["blank"]
    step = 0
    
    current_state = machine["start state"]
    
    while (current_state not in machine["final states"]) or (steps is not None and step <= steps):
        #fill borders with blank symbols (avoid indexError)
        if cursor_position < 0:
            tape.insert(0, blank)
            cursor_position = 0
        elif cursor_position >= len(tape):
            tape.append(blank)
        
        symbol = tape[cursor_position] 

        transition = machine['table'][current_state][symbol]

        history_dict = {
            'state': current_state,
            'reading': symbol,
            'position': cursor_position,
            'memory': ''.join(tape),
            'transition': transition
        }

        history.append(history_dict)

        #cursor_position = moveCurosr(transition, cursor_position)

        #move the cursor
        if transition == "L" or transition == "R":
            cursor_position = moveCurosr(transition, cursor_position)
        else:
            write_symbol = transition.get("write")
            left_transition = transition.get("L")
            right_transition = transition.get("R")

            if write_symbol is not None:
                tape[cursor_position] = write_symbol

            if left_transition is not None:
                cursor_position -= 1
                current_state = left_transition
            elif right_transition is not None:
                cursor_position += 1
                current_state = right_transition
            else:
                cursor_position += 0
                break

        step += 1

    result = "".join(tape).strip(blank)

    accepeted = current_state in machine["final states"]

    return (result, history, accepeted)
